#!/bin/bash
set -e
filepath=$1
find "$filepath" -name "*.asm"|while read fname; do
  echo "assemble $fname"
  "$REF_PATH"/nja "$fname" "$fname".bin
done