#!/bin/bash
export NJVM_VERSION=$(./njvm --version| tail -c 2)
export REF_PATH="$(pwd)/aufgaben/a$NJVM_VERSION"
export TMP_PATH="$(pwd)/tmp/"
echo "$NJVM_VERSION"
chmod +x njvm "$REF_PATH"/njvm "$REF_PATH"/nja "$REF_PATH"/njc ci/autocompile.sh ci/autoassemble.sh

#Copy all tiles in a tmp folder
mkdir "$TMP_PATH"
find "./ci/tests" -name "*.nj"|while read fname; do
  echo "copy $fname"
  cp "$fname" "$TMP_PATH"
done
find "./ci/tests" -name "*.asm"|while read fname; do
  echo "copy $fname"
  cp "$fname" "$TMP_PATH"
done

#compile and assemble
if ! ./ci/autocompile.sh "$TMP_PATH"; then
    echo "compile error"
    exit 1
fi
if ! ./ci/autoassemble.sh "$TMP_PATH"; then
  echo "assemble error"
    exit 2
fi

#run the actual test
find "./tmp" -name "*.bin"|while read fname; do

  #run the programs
  echo -n "test $fname"
  ${REF_PATH}/njvm $fname > "$TMP_PATH/ref.txt"
  REF_exit=$?
  ./njvm $fname > "$TMP_PATH/our.txt"
  OUR_exit=$?

  #check the output
  if cmp -s "$TMP_PATH/ref.txt" "$TMP_PATH/our.txt"; then
    echo ' ✓'
  else
    echo ' x'
    echo 'The output is different'
    echo "Ref:"
    cat "$TMP_PATH/ref.txt"
    echo "Our:"
    cat "$TMP_PATH/our.txt"
    if [ "$REF_exit" -eq "0" ]; then
      exit 3
    else
      echo "but ref exited with $REF_exit, so this is no problem"
      echo "our exit code: $OUR_exit"
    fi
  fi
done

#cleaning
if [ "$1" == "-c" ]; then
  echo "skipped cleaning"
else
  echo "cleaning"
  rm -r tmp
fi
echo "finish"