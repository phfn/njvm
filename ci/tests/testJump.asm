    jmp L1
TRUE:
    pushc 1
    pushc 1
    eq
    popr
    ret
FALSE:
    pushc 1
    pushc 2
    eq
    popr
    ret
L1:
    pushc 1
    wrint
    jmp L2
    pushc 10
    wrint
L2:
    pushc 2
    wrint
    call TRUE
    pushr
    brt L3
    pushc 20
    wrint
L3:
    pushc 3
    wrint
    call FALSE
    pushr
    brt L4
    pushc 4
    wrint
L4:
    call FALSE
    pushr
    brf L5
    pushc 30
    wrint
L5:
    pushc 5
    wrint
    call TRUE
    pushr
    brf L6
    pushc 6
    wrint
L6:
    pushc 78910
    wrint
    pushc 1
    dup
    wrint
    wrint
    halt
