#!/bin/bash
set -e
filepath=$1
find "$filepath" -name "*.nj"|while read fname; do
  echo "compile $fname"
  "$REF_PATH"/njc --output "$fname".asm "$fname"
done