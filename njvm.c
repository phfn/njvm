#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/support.h"
#include "include/bigint.h"

#define VERSION 7
//#define DEBUG

#define TRUE 1
#define FALSE 0

#define ERROR 1
#define STACKOVERFLOW_ERROR 2
#define STACKUNDERFLOW_ERROR 3
#define DIVISION_BY_ZERO_ERROR 4


#define FILE_NOT_FOUND_ERROR 5
#define WRONG_FILE_SIZE_ERROR 6
#define NJBF_ERROR 7
#define WRONG_VERSION_ERROR 8
#define MEMORY_FULL_ERROR 9
#define INVALID_ARGUMENTS_ERROR 10

#define INVALID_JUMP_ERROR 11
#define FRAME_POINTER_INVALID 12
#define INVALID_CALL_ERROR 13
#define BOOLEAN_IS_NOT_TRUE_OR_FALSE 13

#define IS_NOT_AN_OBJ_REF 101
#define IS_AN_OBJ_REF 102
#define BIGINT_ERROR 103

#define STACK_SIZE 1000

#define HALT   0
#define PUSHC  1

#define ADD    2
#define SUB    3
#define MUL    4
#define DIV    5
#define MOD    6

#define RDINT  7
#define WRINT  8
#define RDCHR  9
#define WRCHR 10

#define PUSHG 11
#define POPG  12
#define ASF   13
#define RSF   14
#define PUSHL 15
#define POPL  16

#define EQ    17
#define NE    18
#define LT    19
#define LE    20
#define GT    21
#define GE    22
#define JMP   23
#define BRF   24
#define BRT   25
#define CALL  26
#define RET   27
#define DROP  28
#define PUSHR 29
#define POPR  30
#define DUP   31

#define NEW   32
#define GETF  33
#define PUTF  34
#define NEWA  35
#define GETFA 36
#define PUTFA 37
#define GETSZ 38
#define PUSHN 39
#define REFEQ 40
#define REFNE 41

#define IMMEDIATE(x) ((x) & 0x00FFFFFF)
//immediate belegt nur die letzten 24 bit, die ersten 8 werden auf 0 gesetzt.

#define SIGN_EXTEND(i) ((i) & 0x00800000 ? (i) | 0xFF000000 : (i))
//if (i&0x00800000){i= i | 0xFF000000} else{i=i}
//wenn erstes immediate gesetz (also negative zahl im immediate) -> setzen auch die ersten 8 bit damit die Zahl auch als negativ betrachtet wird

#define MSB (1 << (8 * sizeof(unsigned int) - 1))
#define IS_PRIMITIVE(objRef) (((objRef)->size & MSB) == 0)
#define GET_ELEMENT_COUNT(objRef) ((objRef)->size & ~MSB)
#define GET_REFS_PTR(objRef) ((ObjRef *) (objRef)->data)

char* opCodes[]={"HALT ", "PUSHC", "ADD  ", "SUB  ", "MUL  ", "DIV  ", "MOD  ", "RDINT", "WRINT", "RDCHR", "WRCHR", "PUSHG", "POPG ", "ASF  ", "RSF  ", "PUSHL", "POPL ",
                 "EQ   ", "NE   ", "LT   ", "LE   ", "GT   ", "GE   ", "JMP  ", "BRF  ", "BRT  ", "CALL ", "RET  ", "DROP ", "PUSHR", "POPR ", "DUP y "};

typedef struct{
    int isObjRef;
    union{
        int number;
        ObjRef objRef;
    } value;
} StackSlice;
StackSlice stack [STACK_SIZE];
int sp=0;//Stack Pointer
int fp=0;//frame pointer
ObjRef ret=0;

unsigned int pc=0;//Programm Counter
unsigned int *prog_mem;
unsigned int prog_mem_size;

ObjRef *sda;
unsigned int sda_size;

int get_i(int index){
    if(index>=STACK_SIZE) exit(STACKOVERFLOW_ERROR);
    if(index<0) exit(STACKUNDERFLOW_ERROR);
    StackSlice ss = stack[index];
    if (ss.isObjRef == TRUE) {
        exit(IS_AN_OBJ_REF);
    }
    return ss.value.number;
}

void set_i(int index, int value){
    if(index>=STACK_SIZE) exit(STACKOVERFLOW_ERROR);
    if(index<0) exit(STACKUNDERFLOW_ERROR);
    StackSlice ss = {.isObjRef=FALSE, .value.number=value};
    stack[index] = ss;
}

void push_i(int value){
    set_i(sp, value);
    sp++;
}


int pop_i(){
    int res = get_i(sp-1);
    sp--;

    return res;
}

ObjRef get_o(int index){
    if(index>=STACK_SIZE) exit(STACKOVERFLOW_ERROR);
    if(index<0) exit(STACKUNDERFLOW_ERROR);
    StackSlice ss = stack[index];
    if (ss.isObjRef == FALSE) {
        exit(IS_NOT_AN_OBJ_REF);
    }
    return ss.value.objRef;
}

void set_o(int index, ObjRef value){
    if(index>=STACK_SIZE) exit(STACKOVERFLOW_ERROR);
    if(index<0) exit(STACKUNDERFLOW_ERROR);
    StackSlice ss = {.isObjRef=TRUE, .value.objRef=value};
    stack[index] = ss;
}

void push_o(ObjRef value){
    set_o(sp, value);
    sp++;
}


ObjRef pop_o(){
    if(sp <= 0){
        printf("STACKUNDERFLOW_ERROR\n");
        exit(STACKUNDERFLOW_ERROR);
    }
    ObjRef res = get_o(sp-1);
    sp--;
    return res;
}


ObjRef new_ObjRef(int value){
    ObjRef objRef = malloc(sizeof(int) + sizeof(int));
    *(int *)objRef->data=value;
    objRef->size = sizeof(int);
    return objRef;
}
void fatalError(char *msg) {
    printf("%s", msg);
    exit(BIGINT_ERROR);
}/* print a message and exit */

ObjRef newPrimObject(int dataSize){
    ObjRef objRef = malloc(sizeof(int) + dataSize);
    objRef->size = dataSize;
    return objRef;
}/* create a new primitive object */


ObjRef newCmpObject(int number_of_objects) {
    ObjRef objRef = malloc(sizeof(int) + sizeof(void *) * number_of_objects);
    objRef->size = number_of_objects|MSB;//setze most significant bit als Markierung, dass das ein CmpObject ist
    for (int i = 0; i < number_of_objects; i++) {
        GET_REFS_PTR(objRef)[i]=NULL;
    }
    return objRef;
}

void print_stack(void) {
    printf("\n Stack\n");
    printf(".-------+--------.\n");
    for (int i=sp; i>=0; i--) {
        if (i == sp) {
            printf("|sp->%3d| <empty>|\n", i);
        } else {
            if (i == fp) {
                if (stack[i].isObjRef == TRUE) {
                    bip.op1=get_o(i);
                    printf("|fp->%3d| %6d |\n", i, bigToInt());
                } else {
                    printf("|fp->%3d| %6d |\n", i, stack[i].value.number);
                }
            }else{
                if (stack[i].isObjRef == TRUE) {
                    bip.op1=get_o(i);
                    printf("|%7d| %6d |\n", i, bigToInt());
                } else {
                    printf("|%7d| %6d |\n", i, stack[i].value.number);
                }
            }
        }
    }
    printf("'-------+--------'\n\n");
}

void print_bip(){
    ObjRef bop1 = bip.op1;
    ObjRef bop2 = bip.op2;
    ObjRef bres = bip.res;
    ObjRef brem = bip.rem;
    char op1[5];
    char op2[5];
    char res[5];
    char rem[5];
    if(bip.op1!=NULL){
        sprintf(op1, "%4d", bigToInt());
    }else{
        sprintf(op1, "%s", "NULL");
    }
    bip.op1=bip.op2;
    if(bip.op2!=NULL){
        sprintf(op2, "%4d", bigToInt());
    }else{
        sprintf(op2, "%s", "NULL");
    }
    bip.op1=bip.res;
    if(bip.res!=NULL){
        sprintf(res, "%4d", bigToInt());
    }else{
        sprintf(res, "%s", "NULL");
    }
    bip.op1=bip.rem;
    if(bip.rem!=NULL){
        sprintf(rem, "%4d", bigToInt());
    }else{
        sprintf(rem, "%s", "NULL");
    }
    printf(".---------+------.\n"
           "| bip.op1 | %s |\n"
           "| bip.op2 | %s |\n"
           "| bip.res | %s |\n"
           "| bip.rem | %s |\n"
           "'---------+------'\n", op1, op2, res, rem);
    bip.op1=bop1;
    bip.op2=bop2;
    bip.res=bres;
    bip.rem=brem;

}

unsigned int halt_bool=FALSE;
void exec(unsigned int IR){
    int opcode=IR >> 24;
    int imm=SIGN_EXTEND(IMMEDIATE(IR));
    ObjRef x_o, x_o2, x_o3;

#ifdef DEBUG

    print_stack();
    printf("opCode: %s\n"
           "imm:    %d\n"
           "pc:     %d", opCodes[opcode], imm, pc);
#endif
    switch (opcode) {
        case HALT:
            halt_bool=TRUE;
            break;

        case PUSHC:
            bigFromInt(imm);
            push_o(bip.res);
            break;

        case ADD:
            bip.op2=pop_o();
            bip.op1=pop_o();
            bigAdd();
            push_o(bip.res);
            break;

        case SUB:
            bip.op2=pop_o();
            bip.op1=pop_o();
            bigSub();
            push_o(bip.res);
            break;

        case MUL:
            bip.op2=pop_o();
            bip.op1=pop_o();
            bigMul();
            push_o(bip.res);
            break;

        case DIV:
            bip.op2=pop_o();
            bip.op1=pop_o();
            bigDiv();
            push_o(bip.res);
            break;

        case MOD:
            bip.op2=pop_o();
            bip.op1=pop_o();
            bigDiv();
            push_o(bip.rem);
            break;

        case RDINT:
            bigRead(stdin);
            push_o(bip.res);
            break;

        case WRINT:
            bip.op1 = pop_o();
            bigPrint(stdout);
            break;

        case RDCHR:
            bigFromInt(getchar());
            push_o(bip.op1);
            break;

        case WRCHR:
            bip.op1 = pop_o();
            printf("%c", bigToInt());
            break;

        case PUSHG :
            push_o(sda[imm]);
            break;

        case POPG:
            sda[imm]= pop_o();
            break;

        case ASF:
            if(fp>sp) exit(FRAME_POINTER_INVALID);
            if(sp+imm>STACK_SIZE) exit(STACKOVERFLOW_ERROR);
            if(sp+imm<0) exit(STACKUNDERFLOW_ERROR);
            push_i(fp);
            fp = sp;
            sp = sp + imm;
            break;

        case RSF://TODO Somethin went wrong here
            sp = fp;
            fp = pop_i();
            break;

        case PUSHL:
            push_o(get_o(fp + imm));
            break;

        case POPL:
            set_o(fp + imm, pop_o());
            break;

        case EQ:
            bip.op2=pop_o();
            bip.op1=pop_o();
            if (bigCmp()==0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case NE:
            bip.op2=pop_o();
            bip.op1=pop_o();

            if (bigCmp()!=0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case LT:
            bip.op2=pop_o();
            bip.op1=pop_o();

            if (bigCmp()<0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case LE:
            bip.op2=pop_o();
            bip.op1=pop_o();

            if (bigCmp()<=0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case GT:
            bip.op2=pop_o();
            bip.op1=pop_o();

            if (bigCmp()>0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case GE:
            bip.op2=pop_o();
            bip.op1=pop_o();

            if (bigCmp()>=0){
                bigFromInt(TRUE);
            }
            else{
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case JMP:
            if ((imm > 0) && (imm < prog_mem_size))
                pc = imm;
            else {
                printf("Error: Jump target out of bounds\n");
                exit(INVALID_JUMP_ERROR);
            }
            break;

        case BRF:
            bip.op1=pop_o();
            if(!(bigToInt()==TRUE||bigToInt()==FALSE)) exit(BOOLEAN_IS_NOT_TRUE_OR_FALSE);
            if (bigToInt() == FALSE){
                if ((imm > 0) && (imm < prog_mem_size))
                    pc = imm;
                else {
                    printf("Error: Jump target out of bounds\n");
                    exit(INVALID_JUMP_ERROR);
                }
            }
            break;

        case BRT:
            bip.op1=pop_o();
            if(!(bigToInt()==TRUE||bigToInt()==FALSE)) exit(BOOLEAN_IS_NOT_TRUE_OR_FALSE);

            if (bigToInt() == TRUE){
                if ((imm > 0) && (imm < prog_mem_size))
                    pc = imm;
                else {
                    printf("Error: Jump target out of bounds\n");
                    exit(INVALID_JUMP_ERROR);
                }
            }
            break;
            
        case CALL:
            push_i(pc);

            if ((imm > 0) && (imm < prog_mem_size))
                pc = imm;
            else {
                printf("Error: Call target out of bounds\n");
                exit(INVALID_CALL_ERROR);
            }
            break;
        	
        case RET:
            pc = pop_i();
            break;
        
        case DROP:
            for (int i=0; i<imm; i++)
                pop_o();
            break;

        case PUSHR:
            push_o(ret);
            break;

        case POPR:
            ret = pop_o();
            break;

        case DUP:
            //TODO mal gucken ob das jetzt richtig dupliziert ist. Man kännte auch den wert auslsen und zwei neue  Objrecte erstellen...
            x_o = pop_o();
            push_o(x_o);
            push_o(x_o);
            break;

        case NEW:
            push_o(newCmpObject(imm));
            break;

        case GETF:
            push_o(GET_REFS_PTR(pop_o())[imm]);
            break;

        case PUTF:
            x_o = pop_o();  //value
            x_o2 = pop_o(); //cmpobj
            GET_REFS_PTR(x_o2)[imm] = x_o;
            break;

        case NEWA:
            bip.op1 = pop_o();
            push_o(newCmpObject(bigToInt()));
            break;

        case GETFA:
            x_o = pop_o(); //index
            x_o2 = pop_o(); //array
            bip.op1 = x_o;
            push_o(GET_REFS_PTR(x_o2)[bigToInt()]);
            break;

        case PUTFA:
            x_o = pop_o();  //value
            x_o2 = pop_o(); //index
            x_o3 = pop_o(); //array
            bip.op1=x_o2;   //index als integer
            GET_REFS_PTR(x_o3)[bigToInt()] = x_o;
            break;
        case GETSZ:
            x_o = pop_o();
            if (IS_PRIMITIVE(x_o)) {
                bigFromInt(-1);
            } else {
                bigFromInt(GET_ELEMENT_COUNT(x_o));
            }
            push_o(bip.res);
            break;

        case PUSHN:
            push_o(NULL);
            break;

        case REFEQ:
            if (pop_o() == pop_o()) {
                bigFromInt(TRUE);
            } else {
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        case REFNE:
            if (pop_o() != pop_o()) {
                bigFromInt(TRUE);
            } else {
                bigFromInt(FALSE);
            }
            push_o(bip.res);
            break;

        default:
            printf("ERROR NOT IMPLEMENTED YET");
    }
}


void load_prog_memory(char *filepath){
    FILE *file;
    file = fopen(filepath, "r");

    if (!file){
        printf("Error: cannot open code file '%s'\n", filepath);
        exit(FILE_NOT_FOUND_ERROR);
    }


    unsigned int njbf;
    if (fread(&njbf, sizeof(unsigned int), 1, file) != 1){
        printf("Error: cannot read code file '%s'\n", filepath);
        exit(WRONG_FILE_SIZE_ERROR);
    }
    if (njbf != 0x46424a4e){
        printf("Error: file '%s' is not a Ninja binary\n", filepath);
        exit(NJBF_ERROR);
    }


    unsigned version_file;
    if (fread(&version_file, sizeof(unsigned int), 1, file) != 1){
        printf("Error: cannot read code file '%s'\n", filepath);
        exit(WRONG_FILE_SIZE_ERROR);
    }
    if (version_file != VERSION){
        printf("Error: file '%s' has wrong version_file number\n", filepath);
        exit(WRONG_VERSION_ERROR);
    }


    if (fread(&prog_mem_size, sizeof(unsigned int), 1, file) != 1){
        printf("Error: cannot read code file '%s'\n", filepath);
        exit(WRONG_FILE_SIZE_ERROR);
    }

    prog_mem = malloc(sizeof(unsigned int) * prog_mem_size);
    if (!prog_mem){
        printf("Error: No free heap memory\n");
        exit(MEMORY_FULL_ERROR);
    }


    if (fread(&sda_size, sizeof(unsigned int), 1, file) != 1){
        printf("Error: cannot read code file '%s'\n", filepath);
        exit(WRONG_FILE_SIZE_ERROR);
    }

    sda = malloc(sizeof(unsigned int) * sda_size);
    if (!sda){
        printf("Error: No free heap memory\n");
        exit(MEMORY_FULL_ERROR);
    }


    for (int i=0; i<prog_mem_size; i++){
        if (fread(&prog_mem[i], sizeof(unsigned int), 1, file) != 1) {
            printf("Error: cannot read code file '%s'\n", filepath);
            exit(WRONG_FILE_SIZE_ERROR);
        }
    }

    fclose(file);
}

void print_prog_mem(){
    int opcode;
    unsigned int IR;
    int imm;
    for(int i=0; i < prog_mem_size; i++) {
        IR = prog_mem[i];
        opcode = IR >> 24;
        imm = SIGN_EXTEND(IMMEDIATE(IR));
        if(i==pc){
            printf("[%2d]: %s %3d <---pc\n", i , opCodes[opcode], imm);
        }else{
            printf("[%2d]: %s %3d\n", i , opCodes[opcode], imm);
        }
    }
}


void run(){
    unsigned int IR;
    while(!halt_bool){

        IR=prog_mem[pc];
        pc=pc+1;
        exec(IR);

    }
}

void print_data(){
    for(int i=0; i< sda_size; i++){
        printf("|%2d| %6d |\n", i, *(int *)sda[i]->data);
    }
}



void debug(){
    int opcode;
    unsigned int IR;
    int imm;
    int breakpoint=-1;
    printf("Welcome to the njvm Debugger\n Please use only the first character of any command. i for inspect, b17r for breakpoint at 17 und run\n");
    while(!halt_bool){
        IR = prog_mem[pc];
        opcode = IR >> 24;
        imm = SIGN_EXTEND(IMMEDIATE(IR));
        printf("[%2d]: %s %3d\n", pc , opCodes[opcode], imm);
        printf("(i)nspect, (l)ist, (b)reakpoint, (s)tep, (r)un, (q)uit?\n");
        char c;
        scanf(" %c",&c);
        switch (c){
            case 'i'://inspect
                printf("(s)tack or (d)ata?\n");
                scanf(" %c",&c);
                switch (c) {
                    case 's'://stack
                        print_stack();
                        break;
                    case 'd'://data
                        print_data();
                        break;
                }
                break;
            case 'l'://list
                print_prog_mem();
                break;
            case 'b'://breakpoint
                printf("address to set, -1 to clear, now its %d\n", breakpoint);
                scanf(" %d", &breakpoint);
                printf("set to %d\n", breakpoint);
                break;
            case 's'://step
                pc=pc+1;
                exec(IR);
                break;
            case 'r'://run
                while(!halt_bool && pc!=breakpoint){

                    IR=prog_mem[pc];
                    pc=pc+1;
                    exec(IR);

                }
                break;

            case  'q'://quit
                halt_bool=TRUE;
                break;

        }

    }
}
int main(int argc, char* argv[]){
    int debugFlag=FALSE;
    if (argc > 1){
        for(int i=1; i< argc; i++){
            if (strcmp(argv[i], "--help") == 0){
                printf("usage: ../njvm [option] [option] file ...\n"
                       "  --version                            show version and exit\n"
                       "  --help                               show this help and exit\n"
                       "  --assemble <inputfile> <outputfile>   compile an asm file to bin and run the bin\n"
                       "  file                                 run program");
                exit(0);
            }else if (strcmp(argv[i], "--version") == 0) {
                printf("Ninja Virtual Machine version %d\n", VERSION);
                exit(0);
            }else if (strcmp(argv[i], "--test") == 0) {

                bigFromInt(15);
                bip.op1 = bip.res;
                bigFromInt(27);
                bip.op2 = bip.res;
                bigAdd();
                bip.op1 = bip.res;

                printf("%d", bigToInt());
                printf("%d", pc);
                exit(0);
            }else if (strcmp(argv[i], "--assemble") == 0) {
                char* input_file = argv[i + 1];
                char* output_file = argv[i+2];
                char nja_path[128];
                char chmod_cmd[256];
                char nja_cmd[512];
                sprintf(nja_path, "aufgaben/a%d/nja", VERSION);
                sprintf(chmod_cmd, "chmod +x %s", nja_path);
                system(chmod_cmd);
                sprintf(nja_cmd, "./%s %s %s", nja_path, input_file, output_file);
                system(nja_cmd);
                i++;
            }else if (strcmp(argv[i], "--debug") == 0) {
                debugFlag=TRUE;
            }else if (argv[i][0] == '-'){
                printf("unknown command line argument '%s', try '%s --help'\n", argv[1], argv[0]);
                exit(INVALID_ARGUMENTS_ERROR);
            }
            else{
                load_prog_memory(argv[i]);
            }
        }
    }else{
        printf("try --help");
        exit(INVALID_ARGUMENTS_ERROR);
    }
    //print_prog_mem();
    if (debugFlag == FALSE) {
        #ifdef DEBUG
            print_prog_mem();
        #endif
        printf("Ninja Virtual Machine started\n");
        run();
        printf("Ninja Virtual Machine stopped\n");
    }
    else{
        debug();
    }
    return 0;
}
